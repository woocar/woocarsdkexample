//
//  WoocarSDKExampleApp.swift
//  WoocarSDKExample
//
//  Created by Joaquin Enriquez on 9/17/20.
//

import SwiftUI
import WoocarSDK

class AppDelegate: NSObject, UIApplicationDelegate {
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        Woocar.sharedInstance.initialize("YOUR_WOOCAR_API_KEY")
        return true
    }
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        print("applicationWillTerminate")
        Woocar.sharedInstance.applicationWillTerminate()
    }
}

@main
struct WoocarSDKExampleApp: App {
    
    // inject into SwiftUI life-cycle via adaptor !!!
    @UIApplicationDelegateAdaptor(AppDelegate.self) var appDelegate

    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
