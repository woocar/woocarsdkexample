//
//  ContentView.swift
//  TestSdk
//
//  Created by Joaquin Enriquez on 8/18/20.
//  Copyright © 2020 Woolabs. All rights reserved.
//
import SwiftUI
import WoocarSDK

class Model: ObservableObject {
    @Published var loginResult: String = "-"
    @Published var permissions: String = "-"
    func onClickLogin() {
        Woocar.sharedInstance.login("Woocar Test", "user@yourCompany.com") { result in
            self.loginResult = result
        }
    }
    
    func onClickCheckPermissions() {
        self.permissions = "\(Woocar.sharedInstance.isAllPermissionAuthorized())"
        if !Woocar.sharedInstance.isAllPermissionAuthorized() {
            Woocar.sharedInstance.openAppSettings()
        }
    }
}

struct ContentView: View {
    @ObservedObject var model: Model = Model()
    
    var body: some View {
        VStack {
            Button(action: {
                self.model.onClickLogin()
            }) {
                Text("Login").padding()
                .background(Color.black)
                .foregroundColor(Color.white)
                .cornerRadius(6)
            }
            Text(model.loginResult).padding()
            Button(action: {
                self.model.onClickCheckPermissions()
            }) {
                Text("Check permissions").padding()
                .background(Color.black)
                .foregroundColor(Color.white)
                .cornerRadius(6)
            }
            Text("Permissions: \(model.permissions)").padding()
            DriverView()
            TripsView()
        }.padding(10)
        
    }
}

class TripModel: ObservableObject {
    @Published var trips: [Trip] = Woocar.sharedInstance.getTrips()
}

struct TripsView: View {
    @ObservedObject var model: TripModel = TripModel()
    var body: some View {
            NavigationView {
                List {
                    ForEach(model.trips, id: \.id) { trip in
                        VStack {
                            Text("trip id:\(trip.id)- date: \(trip.date)- total: \(trip.totalScore)- duration: \(trip.duration)- distance \(trip.distance)- focus:\(trip.focusScore) - alerts: \(trip.alerts.count)")
                            ForEach(trip.alerts, id: \.date) { alert in
                                Text("alert \(alert.date)-\(alert.type)-\(alert.longitude)-\(alert.latitude)-\(alert.duration)-\(alert.speed)")
                            }
                        }
                        
                        
                    }
                }
                .navigationBarTitle("Trips")
            }
    }
}


struct DriverView: View {
    
    var body: some View {
        let driver = Woocar.sharedInstance.getDriver()
        return Text("Driver \(driver.id)-\(driver.name)-\(driver.email)")
    }
    
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        TripsView()
    }
}
