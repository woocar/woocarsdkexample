# WoocarSDKExample #

How to integrate woocar sdk to your ios project.
Yo need to have an account and a valid API_KEY.
To contact sales, visit https://woocar.io

### How do I get set up?

#### 1. Update Podfile dependencies

```
platform :ios, '10.0'

target 'WoocarSDKExample' do
  use_frameworks!
  pod 'WoocarSDK', '~> 0.0.20'
  pod 'RealmSwift', '~> 5.4.2'
end
```

Run:
```
pod install --repo-update
```

#### 2. Enable **Background Mode** capability to your project

Check:
* Location Updates
* Background fetche

![Alt text](docs/woocar-1.png)


#### 3. Info.list - Ask for permissions description

Since sdk required user permissions, we need to explain to user why we need it.

```
    <key>NSLocationAlwaysAndWhenInUseUsageDescription</key>
    <string>Please select always allow. This will enable us to automagically record trips without you doing anything. If you do not allow this, no trips will be recorded. If the option is not available, select While using the app and you will be shown this confirmation screen later on.</string>
    <key>NSLocationAlwaysUsageDescription</key>
    <string>Please select always allow. This will enable us to automagically record trips without you doing anything. If you do not allow this, no trips will be recorded. If the option is not available, select While using the app and you will be shown this confirmation screen later on.</string>
    <key>NSLocationWhenInUseUsageDescription</key>
    <string>Please select always allow. This will enable us to automagically record trips without you doing anything. If you do not allow this, no trips will be recorded. If the option is not available, select While using the app and you will be shown this confirmation screen later on.</string>
    <key>NSMotionUsageDescription</key>
    <string>Please select OK!. We need access to movement data in order to automagically detect when you are in a vehicle!. If you do not allow this, no trips will be detected.</string>
```

#### 4. AppDelegate

In order to get well integrated you must call our methods in the right place in the app lifecycle

```
import WoocarSDK

class AppDelegate: UIApplicationDelegate {
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        Woocar.sharedInstance.initialize("YOUR_WOOCAR_API_KEY")
        return true
    }
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        print("applicationWillTerminate")
        Woocar.sharedInstance.applicationWillTerminate()
    }
}

```

### Enable feature to an user - Sign in

To start tracking a new driver, user should sign in to the sdk.
```
    func onClickLogin() {
        Woocar.sharedInstance.login("Woocar Test", "user@yourCompany.com") { result in
            self.loginResult = result
        }
    }

```
Once the user is successfully logged motion and location permissions are required to the user.
If the user does not accept it, woocar won't be able to track his driving performance.

### Check if user give permissions:

To check if the user accepted permissions call the method bellow:
`Woocar.sharedInstance.isAllPermissionAuthorized()`

To open app settings to enable permissions:
`Woocar.sharedInstance.openAppSettings()`

* With both methods above you are able to check if the user gives permission and also provides a way to open the app settings and let the user grant it
```
Example:

func onClickCheckPermissions() {
    self.permissions = "\(Woocar.sharedInstance.isAllPermissionAuthorized())"
    if !Woocar.sharedInstance.isAllPermissionAuthorized() {
        Woocar.sharedInstance.openAppSettings()
    }
}
```

### Accessing trip data

**Woocar.sharedInstance.getTrips()**
Returns an array of finished trips

```
var trips:[Trip] = Woocar.sharedInstance.getTrips()
```

### Data model

```
public class Trip: Codable {
    /* user props. */
    public var id: String = ""
    public var date: Date = Date()
    
    /* Scores. */
    public var totalScore: Int = 0
    public var controlScore: Int = 0
    public var focusScore: Int = 0
    public var cautionScore: Int = 0
    public var ecoScore: Int = 0
    
    public var averageSpeed: Double = 0
    public var duration: Int = 0
    public var distance: Double = 0.0
    
    public var firstLatitude: Double = 0.0
    public var firstLongitude: Double = 0.0
    
    public var lastLatitude: Double = 0.0
    public var lastLongitude: Double = 0.0
    
    public var alerts: [Alert] = []
    
    public var finished: Bool = false
}

public class Alert: Codable {
    public var type: String = ""
    public var date: Date = Date()
    public var duration: Int = 0
    
    // location data
    public var longitude: Double = 0
    public var latitude: Double = 0
    
    // speed alerts
    public var speed: Double = 0
}

```



